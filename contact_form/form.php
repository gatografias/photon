<form id="mailform" action="<?php echo html_encode(getRequestURI()); ?>" method="post" accept-charset="UTF-8">
	<input type="hidden" id="sendmail" name="sendmail" value="sendmail" />
	<?php
	$star = '<strong>*</strong>';
	if (showOrNotShowField(getOption('contactform_title'))) {
		?>
		<div class="row form-group">
			<div class="col-md-12">
				<label class="text-black" for="title"><?php printf(gettext("Title%s"), checkRequiredField(getOption('contactform_title'))); ?></label>
				<input class="form-control" type="text" id="title" name="title" size="50" value="<?php echo html_encode($mailcontent['title']); ?>"<?php if ($_processing_post) echo ' disabled="disabled"'; ?> />
			</div>
		</div>
		<?php
	}
	if (showOrNotShowField(getOption('contactform_name'))) {
		?>
		<div class="row form-group">
            <div class="col-md-12">
				<label class="text-black" for="name"><?php printf(gettext("Name%s"), checkRequiredField(getOption('contactform_name'))); ?></label>
				<input class="form-control" type="text" id="name" name="name" size="50" value="<?php echo html_encode($mailcontent['name']); ?>"<?php if ($_processing_post) echo ' disabled="disabled"'; ?> />
			</div>
		</div>
		<?php
	}
	?>
	<div class="row form-group" style="display:none;">
        <div class="col-md-12">		
			<label class="text-black" for="username">Username:</label>
			<input class="form-control" type="text" id="username" name="username" size="50" value="<?php echo html_encode($mailcontent['honeypot']); ?>"<?php if ($_processing_post) echo ' disabled="disabled"'; ?> />
		</div>
	</div>
	<?php
	if (showOrNotShowField(getOption('contactform_company'))) {
		?>
		<div class="row form-group">
        	<div class="col-md-12">
				<label class="text-black" for="company"><?php printf(gettext("Company%s"), checkRequiredField(getOption('contactform_company'))); ?></label>
				<input class="form-control" type="text" id="company" name="company" size="50" value="<?php echo html_encode($mailcontent['company']); ?>"<?php if ($_processing_post) echo ' disabled="disabled"'; ?> />
			</div>
		</div>
		<?php
	}
	if (showOrNotShowField(getOption('contactform_street'))) {
		?>
		<div class="row form-group">
        	<div class="col-md-12">
				<label class="text-black" for="street"><?php printf(gettext("Street%s"), checkRequiredField(getOption('contactform_street'))); ?></label>
				<input class="form-control" type="text" id="street" name="street" size="50" value="<?php echo html_encode($mailcontent['street']); ?>"<?php if ($_processing_post) echo ' disabled="disabled"'; ?> />
			</div>
		</div>
		<?php
	}
	if (showOrNotShowField(getOption('contactform_city'))) {
		?>
		<div class="row form-group">
        	<div class="col-md-12">
				<label class="text-black" for="city"><?php printf(gettext("City%s"), checkRequiredField(getOption('contactform_city'))); ?></label>
				<input class="form-control" type="text" id="city" name="city" size="50" value="<?php echo html_encode($mailcontent['city']); ?>"<?php if ($_processing_post) echo ' disabled="disabled"'; ?> />
			</div>
		</div>
		<?php
	}
	if (showOrNotShowField(getOption('contactform_state'))) {
		?>
		<div class="row form-group">
        	<div class="col-md-12">
				<label class="text-black" for="state"><?php printf(gettext("State%s"), checkRequiredField(getOption('contactform_state'))); ?></label>
				<input class="form-control" type="text" id="state" name="state" size="50" value="<?php echo html_encode($mailcontent['city']); ?>"<?php if ($_processing_post) echo ' disabled="disabled"'; ?> />
			</div>
		</div>
		<?php
	}
	if (showOrNotShowField(getOption('contactform_country'))) {
		?>
		<div class="row form-group">
        	<div class="col-md-12">
				<label class="text-black" for="country"><?php printf(gettext("Country%s"), checkRequiredField(getOption('contactform_country'))); ?></label>
				<input class="form-control" type="text" id="country" name="country" size="50" value="<?php echo html_encode($mailcontent['country']); ?>"<?php if ($_processing_post) echo ' disabled="disabled"'; ?> />
			</div>
		</div>
		<?php
	}
	if (showOrNotShowField(getOption('contactform_postal'))) {
		?>
		<div class="row form-group">
        	<div class="col-md-12">
				<label class="text-black" for="postal"><?php printf(gettext("Postal code%s"), checkRequiredField(getOption('contactform_postal'))); ?></label>
				<input class="form-control" type="text" id="postal" name="postal" size="50" value="<?php echo html_encode($mailcontent['postal']); ?>"<?php if ($_processing_post) echo ' disabled="disabled"'; ?> />
			</div>
		</div>
		<?php
	}
	if (showOrNotShowField(getOption('contactform_email'))) {
		?>
		<div class="row form-group">
        	<div class="col-md-12">
				<label class="text-black" for="email"><?php printf(gettext("E-Mail%s"), checkRequiredField(getOption('contactform_email'))); ?></label>
				<input class="form-control" type="text" id="email" name="email" size="50" value="<?php echo html_encode($mailcontent['email']); ?>"<?php if ($_processing_post) echo ' disabled="disabled"'; ?> />
			</div>
		</div>
		<?php
	}
	if (showOrNotShowField(getOption('contactform_website'))) {
		?>
		<div class="row form-group">
        	<div class="col-md-12">
				<label class="text-black" for="website"><?php printf(gettext("Website%s"), checkRequiredField(getOption('contactform_website'))); ?></label>
				<input class="form-control" type="text" id="website" name="website" size="50" value="<?php echo html_encode($mailcontent['website']); ?>"<?php if ($_processing_post) echo ' disabled="disabled"'; ?> />
			</div>
		</div>
		<?php
	}
	if (showOrNotShowField(getOption('contactform_phone'))) {
		?>
		<div class="row form-group">
        	<div class="col-md-12">
				<label class="text-black" for="phone"><?php printf(gettext("Phone%s"), checkRequiredField(getOption('contactform_phone'))); ?></label>
				<input class="form-control" type="text" id="phone" name="phone" size="50" value="<?php echo html_encode($mailcontent['phone']); ?>"<?php if ($_processing_post) echo ' disabled="disabled"'; ?> />
			</div>
		</div>
		<?php
	}
	if (getOption("contactform_captcha") && !$_processing_post) {
		$captcha = $_zp_captcha->getCaptcha(gettext("Enter CAPTCHA<strong>*</strong>"));
		?>
		<div class="row form-group">
        	<div class="col-md-12">	
				<?php
				if (isset($captcha['html']))
					echo $captcha['html'];
				if (isset($captcha['input']))
					echo $captcha['input'];
				if (isset($captcha['hidden']))
					echo $captcha['hidden'];
				?>
			</div>
		</div>
		<?php
	}
	?>
	<div class="row form-group">
        <div class="col-md-12">	
			<label class="text-black" for="subject"><?php echo gettext("Subject<strong>*</strong>"); ?></label>
			<input class="form-control" type="text" id="subject" name="subject" size="50" value="<?php echo html_encode($mailcontent['subject']); ?>"<?php if ($_processing_post) echo ' disabled="disabled"'; ?> />
		</div>
	</div>
	<div class="row form-group mailmessage">
        <div class="col-md-12">	
			<label class="text-black" for="message"><?php echo gettext("Message<strong>*</strong>"); ?></label>
			<textarea class="form-control" cols="30" rows="7" placeholder="<?php echo gettext("Write your notes or questions here..."); ?>" id="message" name="message" <?php if ($_processing_post) echo ' disabled="disabled"'; ?>><?php echo $mailcontent['message']; ?></textarea>
		</div>
	</div>
	<?php 
	if(getOption('contactform_dataconfirmation')) { 
		$dataconfirmation_checked = '';
		if(!empty($mailcontent['dataconfirmation'])) {
			$dataconfirmation_checked = ' checked="checked"';
		} 
		?>
		<div class="row form-group mailmessage">
        	<div class="col-md-12">	
				<label class="text-black" for="dataconfirmation">
					<input type="checkbox" name="dataconfirmation" id="dataconfirmation" value="1"<?php echo $dataconfirmation_checked; if ($_processing_post) echo ' disabled="disabled"'; ?>>
					<?php printDataUsageNotice(); echo '<strong>*</strong>'; ?>
				</label>
			</div>
		</div>
	<?php } 
	if (!$_processing_post) {
		?>
		<div class="row form-group mailmessage">
        	<div class="col-md-12">	
				<input type="submit" class="btn btn-primary py-2 px-4 text-white" value="<?php echo gettext("Send Message"); ?>" />
				<input type="reset" class="btn btn-secondary py-2 px-4 text-white" value="<?php echo gettext("Reset"); ?>" />
			</div>
		</div>
	<?php } ?>
</form>