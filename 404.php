<?php
// force UTF-8 Ø
if (!defined('WEBPATH'))
	die();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="<?php echo LOCAL_CHARSET; ?>">
		<?php zp_apply_filter('theme_head'); ?>
		<?php printHeadTitle(); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<?php photon_loadCSS(); ?>
	</head>

	<body>
		<?php zp_apply_filter('theme_body_open'); ?>

		<div class="site-wrap">
			<header id="header">
				<div class="overlay">
					<h1 class="mb-0">
						<a href="<?php echo html_encode(getSiteHomeURL()); ?>" class="text-black h2 mb-0">
							<?php printGalleryTitle(); ?>
						</a>
					</h1>
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-md-8">
								<div class="intro-text">
									<h1>Error 404</h1>
									<h4>Oops! <?php echo gettext('The page you are looking for does not exist.'); ?></h4>
									<a href="<?php echo getSiteHomeURL(); ?>" class="btn btn-custom btn-lg"><?php echo gettext('Back'); ?></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
		</div>

		<?php photon_loadScripts(); ?>
		<?php zp_apply_filter('theme_body_close'); ?>
	</body>
</html>
