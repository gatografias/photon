<?php
// force UTF-8 Ø
if (!defined('WEBPATH'))
	die();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="<?php echo LOCAL_CHARSET; ?>">
		<?php zp_apply_filter('theme_head'); ?>
		<?php printHeadTitle(); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<?php photon_loadCSS(); ?>
	</head>

	<body>
		<?php zp_apply_filter('theme_body_open'); ?>

		<div class="site-wrap">

			<div class="site-mobile-menu">
				<div class="site-mobile-menu-header">
					<div class="site-mobile-menu-close mt-3">
						<span class="icon-close2 js-menu-toggle"></span>
					</div>
				</div>
				<div class="site-mobile-menu-body"></div>
			</div>

			<?php photon_printMainHeaderNav(); ?>

			<div class="container-fluid" data-aos="fade" data-aos-delay="500">
				<div class="swiper-container images-carousel">
					<div class="swiper-wrapper">
						<?php while (next_album()): // the loop of the top level albums  ?>
							<div class="swiper-slide">
								<div class="image-wrap">
									<div class="image-info">
										<h2 class="mb-3"><?php printAlbumTitle(); ?></h2>
										<a href="<?php echo html_encode(getAlbumURL()); ?>" class="btn btn-outline-white py-2 px-4">More Photos</a>
									</div>
									<?php printAlbumThumbImage(getAnnotatedAlbumTitle()); ?>
								</div>
							</div>
						<?php endwhile; ?>
					</div>
					<div class="swiper-pagination"></div>
					<div class="swiper-button-prev"></div>
					<div class="swiper-button-next"></div>
				</div>
			</div>

			<?php photon_printFooterNav(); ?>

		</div>

		<?php photon_loadScripts(); ?>
		<?php zp_apply_filter('theme_body_close'); ?>
		
	</body>
</html>
