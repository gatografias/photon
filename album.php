<?php
// force UTF-8 Ø
if (!defined('WEBPATH'))
	die();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="<?php echo LOCAL_CHARSET; ?>">
		<?php zp_apply_filter('theme_head'); ?>
		<?php printHeadTitle(); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<?php photon_loadCSS(); ?>
	</head>

	<body>
		<?php zp_apply_filter('theme_body_open'); ?>

		<div class="site-wrap">

			<div class="site-mobile-menu">
				<div class="site-mobile-menu-header">
					<div class="site-mobile-menu-close mt-3">
						<span class="icon-close2 js-menu-toggle"></span>
					</div>
				</div>
				<div class="site-mobile-menu-body"></div>
			</div>

			<?php photon_printMainHeaderNav(); ?>

			<div class="site-section"  data-aos="fade">
				<div class="container-fluid">
					<div class="row justify-content-center">
						<div class="col-md-7">
							<div class="row mb-5">
								<div class="col-12 ">
									<h2 class="site-section-heading text-center"><?php printAlbumTitle(); ?></h2>
								</div>
							</div>
						</div>
					</div>

					<div class="row justify-content-center">
					<?php while (next_album()): ?>
						<div class="album col-sm-6 col-md-4 col-lg-3 col-xl-2 item" data-aos="fade">
							<div class="image-wrap">
								<div class="image-info">
									<h2 class="mb-3"><?php printAlbumTitle(); ?></h2>
									<a href="<?php echo html_encode(getAlbumURL()); ?>" class="btn btn-outline-white py-2 px-4">More Photos</a>
								</div>
								<?php printCustomAlbumThumbImage(getAlbumTitle(), null, 200, 200, 200, 200); ?>
							</div>
						</div>
					<?php endwhile; ?>
					</div>
					
					<br class="clearfloat">
					
					<div class="row" id="lightgallery">
						<?php while (next_image()): ?>
							<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2 item" data-aos="fade" data-src="<?php echo html_encode(pathurlencode(getFullImageURL())); ?>" data-sub-html="<h4><?php printBareImageTitle(); ?></h4><p><?php printImageDesc(); ?></p>">
								<a href="#">
									<img src="<?php echo html_encode(pathurlencode(getImageThumb()));?>" alt="<?php printBareImageTitle();?>" class="img-fluid2">
								</a>
							</div>
						<?php endwhile; ?>
					</div> 
					<?php printPageListWithNav(gettext("prev"), gettext("next"),false,true,'row justify-content-center paginacion',NULL,true,7);  ?>
				</div>
			</div>

			<?php photon_printFooterNav(); ?>

		</div>

		<?php photon_loadScripts(); ?>
		<?php zp_apply_filter('theme_body_close'); ?>
		
	</body>
</html>	