<?php
//	Required plugins:
require_once(SERVERPATH . '/' . ZENFOLDER . '/' . PLUGIN_FOLDER . '/tinymce4/plugins/tinyzenpage/tinyzenpage-functions.php');

/**
 * Prints the scripts needed for the header
 */
function photon_loadScripts() {
	global $_zp_themeroot;
	?>
	<script src="<?php echo $_zp_themeroot; ?>/js/jquery-3.3.1.min.js"></script>
	<script src="<?php echo $_zp_themeroot; ?>/js/jquery-migrate-3.0.1.min.js"></script>
	<script src="<?php echo $_zp_themeroot; ?>/js/jquery-ui.js"></script>
	<script src="<?php echo $_zp_themeroot; ?>/js/popper.min.js"></script>
	<script src="<?php echo $_zp_themeroot; ?>/js/bootstrap.min.js"></script>
	<script src="<?php echo $_zp_themeroot; ?>/js/owl.carousel.min.js"></script>
	<script src="<?php echo $_zp_themeroot; ?>/js/jquery.stellar.min.js"></script>
	<script src="<?php echo $_zp_themeroot; ?>/js/jquery.countdown.min.js"></script>
	<script src="<?php echo $_zp_themeroot; ?>/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo $_zp_themeroot; ?>/js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo $_zp_themeroot; ?>/js/swiper.min.js"></script>
	<script src="<?php echo $_zp_themeroot; ?>/js/aos.js"></script>

	<script src="<?php echo $_zp_themeroot; ?>/js/picturefill.min.js"></script>
	<script src="<?php echo $_zp_themeroot; ?>/js/lightgallery-all.min.js"></script>
	<script src="<?php echo $_zp_themeroot; ?>/js/jquery.mousewheel.min.js"></script>

	<script src="<?php echo $_zp_themeroot; ?>/js/main.js"></script>
	
	<script>
		$(document).ready(function(){
		$('#lightgallery').lightGallery();
		});
	</script>
	<?php
}

function photon_loadCSS(){
	global $_zp_themeroot;
	?>
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300i,400,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $_zp_themeroot; ?>/fonts/icomoon/style.css">
    <link rel="stylesheet" href="<?php echo $_zp_themeroot; ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $_zp_themeroot; ?>/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo $_zp_themeroot; ?>/css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo $_zp_themeroot; ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $_zp_themeroot; ?>/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo $_zp_themeroot; ?>/css/lightgallery.min.css">    
    <link rel="stylesheet" href="<?php echo $_zp_themeroot; ?>/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php echo $_zp_themeroot; ?>/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="<?php echo $_zp_themeroot; ?>/css/swiper.css">
    <link rel="stylesheet" href="<?php echo $_zp_themeroot; ?>/css/aos.css">
	<link rel="stylesheet" href="<?php echo $_zp_themeroot; ?>/css/style.css">
	<?php
}

function getPagesLink() {
	return zp_apply_filter('getLink', rewrite_path(_PAGES_ . '/', "/index.php?p=pages"), 'pages.php', NULL);
}

/**
 * Prints the image/subalbum count for the album loop
 */
function photon_printMainHeaderNav() {
	global $_zp_gallery_page, $_zp_zenpage, $_zp_current_album, $_zp_themeroot;
	$context = get_context(); // 1 = Home, 3 = Album, 12289 = Page	
	set_context(ZP_INDEX);
	?>
	<header class="site-navbar py-3" role="banner">
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-6 col-xl-2" data-aos="fade-down">
					<h1 class="mb-0">
						<a href="<?php echo html_encode(getSiteHomeURL()); ?>" class="text-black h2 mb-0" alt="<?php echo printGalleryTitle(); ?>">
							<?php printGalleryTitle(); ?>
						</a>
					</h1>
				</div>
				<div class="col-10 col-md-8 d-none d-xl-block" data-aos="fade-down">
					<nav class="site-navigation position-relative text-right text-lg-center" role="navigation">
						<ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
							<li>
								<a href="<?php echo html_encode(getSiteHomeURL()); ?>"><?php echo gettext('Home'); ?></a>
							</li>
							<li class='has-children'>
								<a href="<?php echo "#"; ?>"><?php echo gettext('Gallery'); ?></a>
								<ul class="dropdown">
								<?php while (next_album()): // the loop of the top level albums  ?>
									<li>
										<a href="<?php echo html_encode(getAlbumURL()); ?>"><?php printAlbumTitle(); ?></a>
									</li>
								<?php endwhile; ?>
								</ul>
							</li>
							<li>
								<a href="<?php echo getCustomPageURL('news'); ?>"><?php echo gettext('Blog'); ?></a>
							</li>
							<?php while (next_page()): // the loop of the pages  ?>
								<li>
									<a href="<?php echo html_encode(getPageURL()); ?>"><?php printPageTitle(); ?></a>
								</li>
							<?php endwhile; ?>
							<li>
								<a href="<?php echo getCustomPageURL('contact'); ?>"><?php echo gettext('Contact'); ?></a>
							</li>
						</ul>
					</nav>
				</div>
				<div class="col-6 col-xl-2 text-right" data-aos="fade-down">
					<div class="d-none d-xl-inline-block">
						<ul class="site-menu js-clone-nav ml-auto list-unstyled d-flex text-right mb-0" data-class="social">

							<li>
								<a href="https://www.instagram.com/gatografias" class="pl-3 pr-3" target="_blank">
									<span class="icon-instagram"></span>
								</a>
							</li>
							<li>
								<a href="https://www.flickr.com/photos/natashastark/" class="pl-3 pr-3" target="_blank">
									<span class="icon-flickr"></span>
								</a>
							</li>
							<li>
								<a href="https://twitter.com/gatografias" class="pl-3 pr-3" target="_blank">
									<span class="icon-twitter"></span>
								</a>	
							</li>
							<!-- <li>
								<a href="https://www.facebook.com/gatografiasdepora" class="pl-0 pr-3" target="_blank">
									<span class="icon-facebook"></span>
								</a>
							</li> -->
						</ul>
					</div>
					<div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;">
						<a href="#" class="site-menu-toggle js-menu-toggle text-black">
							<span class="icon-menu h3"></span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</header>
	<?php
	set_context($context);
}

/**
 * Prints the footer
 */
function photon_printFooterNav() {
	global $_zp_gallery_page, $_zp_current_album;
	?>
	<div class="footer py-4">
		<div class="container-fluid">
			<p>
			<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
			Copyright &copy;<script>document.write(new Date().getFullYear());</script> Gatografías. <?php echo gettext('All rights reserved | This template is made with '); ?> <i class="icon-heart-o" aria-hidden="true"></i> <?php echo gettext(' by '); ?> <a href="https://colorlib.com" target="_blank" >Colorlib</a>
			<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
			</p>
		</div>
	</div>
	<?php
}

function cleanString($text) {
    $utf8 = array(
        '/[áàâãªä]/u'   =>   'a',
        '/[ÁÀÂÃÄ]/u'    =>   'A',
        '/[ÍÌÎÏ]/u'     =>   'I',
        '/[íìîï]/u'     =>   'i',
        '/[éèêë]/u'     =>   'e',
        '/[ÉÈÊË]/u'     =>   'E',
        '/[óòôõºö]/u'   =>   'o',
        '/[ÓÒÔÕÖ]/u'    =>   'O',
        '/[úùûü]/u'     =>   'u',
        '/[ÚÙÛÜ]/u'     =>   'U',
        '/ç/'           =>   'c',
        '/Ç/'           =>   'C',
        '/ñ/'           =>   'n',
        '/Ñ/'           =>   'N',
        '/–/'           =>   '-', // UTF-8 hyphen to "normal" hyphen
        '/[’‘‹›‚]/u'    =>   ' ', // Literally a single quote
        '/[“”«»„]/u'    =>   ' ', // Double quote
        '/ /'           =>   ' ', // nonbreaking space (equiv. to 0x160)
    );
    return preg_replace(array_keys($utf8), array_values($utf8), $text);
}
?>