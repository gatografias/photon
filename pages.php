<?php
// force UTF-8 Ø
if (!defined('WEBPATH'))
	die();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="<?php echo LOCAL_CHARSET; ?>">
		<?php zp_apply_filter('theme_head'); ?>
		<?php printHeadTitle(); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<?php photon_loadCSS(); ?>
	</head>

	<body>
		<?php zp_apply_filter('theme_body_open'); ?>

		<div class="site-wrap">

			<div class="site-mobile-menu">
				<div class="site-mobile-menu-header">
					<div class="site-mobile-menu-close mt-3">
						<span class="icon-close2 js-menu-toggle"></span>
					</div>
				</div>
				<div class="site-mobile-menu-body"></div>
			</div>

			<?php photon_printMainHeaderNav(); ?>

			<div class="site-section"  data-aos="fade">
				<div class="container-fluid">
					<div class="row justify-content-center">
						<div class="col-md-7">
							<div class="row mb-5">
								<div class="col-12 ">
									<h2 class="site-section-heading text-center"><?php printPageTitle(); ?></h2>
								</div>
							</div>
							<?php printPageContent(); ?>
						</div>
					</div>
				</div>
			</div>
			<?php photon_printFooterNav(); ?>

		</div>

		<?php photon_loadScripts(); ?>
		<?php zp_apply_filter('theme_body_close'); ?>
		
	</body>
</html>
